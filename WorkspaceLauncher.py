# This file contains all launcher code.
# The Launcher scans specified directory and gathers a list of 'workspaces'
# 'workspaces' are a directory containing 1 or more executables
# 'workspaces' can be executed as a whole, or by their actions.
# Executing an action simply starts a subprocess.
#
# Tristan Carlson, tristanc97@gmail.com, Sept. 8 2020

import os
import pathlib
import subprocess
import ctypes

class Launcher:

    def __init__(self):
        self._check_root()
        # {'ws_name': [('action1', path/to/action1'), ('action2', 'path/to/action2'), ...], ...}
        self.workspaces = {}
        self.sub_proc_out = open("/dev/null", "w")
        # Get Workspaces dir.  Create it if it does not exist.
        home = os.path.expanduser("~")
        self.ws_dir = os.getenv('WS_DIR', home + '/.WorkspaceLauncher')

        if not os.path.isdir(self.ws_dir):
            pathlib.Path(self.ws_dir).mkdir(parents=True, exist_ok=True)

        # Walk Workspaces dir, put all shell scripts/ws names in dict
        self._parse_workspaces()


    def _parse_workspaces(self):
        for workspace_name in os.listdir(self.ws_dir):
            ws_path = self.ws_dir + '/' + workspace_name
            if os.path.isdir(ws_path):
                self.workspaces[workspace_name] = []
                for shell_script in os.listdir(ws_path):
                    name = shell_script.replace('.sh', '').replace('.bat', '')
                    path = ws_path + '/' + shell_script
                    entry = (name, path)
                    self.workspaces[workspace_name].append(entry)

    """
    Ensure this script is not being run as root.
    We are blindly calling _any_ executable found in an arbitrarily specified 
    dirrectory. 
    """
    def _check_root(self):
        root = None
        try:
            root = os.getuid() == 0
        except AttributeError:
            pass
        try:
            root = ctypes.windll.shell32.IsUserAnAdmin() == 1
        except AttributeError:
            pass

        if root == None:
            print("Error! Could not determine admin status. Exiting")
            exit(-2)
            
        if root:
            print("Error! This script is being run as superuser!  Try again as regular user. Exiting")
            exit(-1)


    def _launch_script(self, path):
        try:

            subprocess.Popen(path, shell=False, stdout=self.sub_proc_out, stderr=self.sub_proc_out)
        except OSError as e:
            print("error starting script '" + path + "'")
            print(e)
            return e


    def get_workspaces(self):
        return self.workspaces


    def get_workspace_names(self):
        ret = []
        for key in self.workspaces:
            ret.append(key)
        return ret


    def execute_workspace(self, ws_name):
        print("executing workspace", ws_name + "...")
        errors = []
        for entry in self.workspaces[ws_name]:
            print("Starting", entry[0] + "...") 
            res = self._launch_script(entry[1])
            if res:
                errors.append(res)

        if len(errors) > 0:

            s = "Error while starting Workspace '" + ws_name +"':"
            for e in errors:
                s = s + "\n" + str(e)
            return s


    def execute_action(self, ws_name, action_name):
        ws = self.workspaces[ws_name]
        action_cmd = None
        for action in ws:
            if action[0] == action_name:
                action_cmd = action[1]
                break

        if action_cmd == None:
            print("Unkown action command!")
            raise ValueError

        print("starting", action_name + "...")
        
        res = self._launch_script(action_cmd)
        if res != None:
            return "Error starting action '" + action_name + "':\n" + str(res)


