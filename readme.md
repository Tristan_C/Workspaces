# Workspaces

## About

Application to open groups of tasks or applications.

tasks are simple shell scripts (but could be any type of executable) that are grouped into workspaces according to the name of their parent directory.

This Application was only tested on Fedora 32 under Gnome, but *should* be compatible with any OS capable of running Python3 + PyQt5 

## Installing on Linux/macos/*nix
* Download this Project.
* launch a terminal and navigate to `Workspaces/install` ex. `cd /home/Jane/Downloads/Workspaces/install`
* type `./install` and hit enter
* Enter administrative password when prompted.
* now Workspaces should be in the path and can be called by entering `workspaces` 

__Note:__ On Gnome a menu entry will also be created by `install.sh`.

## Installing for Windows
_not currently implemented, Although this program \*should\* be able to be executed on a windows machine (I have not tested it.)_

## Usage
Any directories in `~/WorkspaceLauncher/` are considered 'workspaces', and any executable files (text or otherwise) will be considered 'actions'

Example File structure:


![File structure](res/fs.png)

Program view:


![File structure](res/ex.png)


contents of `/home/tristan/.WorkspaceLauncher/Cmpt415 Assignment/Open Clion`:

`#!/bin/bash`
`clion /home/tristan/dev/cmpt/cmpt415/assignment`
 
 


## Icon Credits
### Settings icon from [png fuel](https://www.pngfuel.com/free-png/rhhai)

### Application icon from [Freepik](https://www.flaticon.com/authors/freepik) from [flaticon](https://www.flaticon.com/)
