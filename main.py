# This file handles creation and interaction of the GUI
# Tristan Carlson, tristanc97@gmail.com, Sept. 8 2020


from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import Qt
from PyQt5.Qt import QStandardItemModel, QStandardItem
from PyQt5.QtGui import QFont, QColor
import sys
from WorkspaceLauncher import Launcher


"""
Class for easy creation of tree nodes.
"""
class StandardItem(QStandardItem):
    def __init__(self, txt='', font_size=12, set_bold=False, color=QColor(0, 0, 0)):
        super().__init__()
 
        fnt = QFont('Open Sans', font_size)
        fnt.setBold(set_bold)
 
        self.setEditable(False)
        self.setForeground(color)
        self.setFont(fnt)
        self.setText(txt)


class ErrorDialog(QtWidgets.QDialog):

    def __init__(self, *args, **kwargs):
        # Pull out args THIS class needs before passing remaining args to super-class
        self.msg = kwargs["msg"]
        del kwargs["msg"]

        super(ErrorDialog, self).__init__(*args, **kwargs)
        
        self.setWindowTitle("Error!")
        # Setup OK button.
        QBtn = QtWidgets.QDialogButtonBox.Ok
        self.buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        # Setup window text
        self.label = QtWidgets.QLabel()
        self.label.setText(self.msg)
        self.label.adjustSize()
        # Make window layout and add button and text to it.
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)
        # autosze window to contents.
        self.adjustSize()


"""
Main GUI Class
"""
class Workspaces(QtWidgets.QMainWindow):
    def __init__(self):
        super(Workspaces, self).__init__()
        # Load UI File.
        uic.loadUi('UI/form.ui', self)
        # Initiate Launcher class.  Handles parsing workspace data.
        self.launcher = Launcher()
        # Create handles to important ui elements.
        self.settings_button = self.findChild(QtWidgets.QPushButton, 'settingsButton')
        self.scroll_view = self.findChild(QtWidgets.QScrollArea, 'scrollArea')
        self.tree_view = self.findChild(QtWidgets.QTreeView, 'treeView')
        # Setup ui (populate views etc)
        self._setup_ui()
        self.show()

    def _setup_ui(self):
        self.settings_button.clicked.connect(self._settingsButtonPressed)
        self._construct_tree_view()
        self.tree_view.expandAll()

    def _settingsButtonPressed(self):
        print('settingsButtonPressed')

    def _construct_tree_view(self):
        tree_model = QStandardItemModel()
        root_node = tree_model.invisibleRootItem()

        data = self.launcher.get_workspaces()
        for ws in data:
            ws_root = StandardItem(ws, 16, set_bold=True)
            for action in data[ws]:
                node = StandardItem(action[0], 14)
                ws_root.appendRow(node)

            root_node.appendRow(ws_root)

        self.treeView.setHeaderHidden(True)
        self.tree_view.setModel(tree_model)
        self.tree_view.clicked.connect(self._tree_clicked)

    def _tree_clicked(self, val):
        parent = val.parent().data()
        item = val.data()

        # Check if we are launching a workspace, or action.
        err = None
        if parent != None:
            # Action
            err = self.launcher.execute_action(parent, item)
        else:
            # Workspace
            err = self.launcher.execute_workspace(item)

        if err:
            # Raise Error Dialog.
            dlg = ErrorDialog(msg=err)
            dlg.exec_()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Workspaces()
    app.exec_()
