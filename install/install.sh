#!/bin/bash
WSDIR=$HOME/.WorkspaceLauncher
INSTALLDIR="/opt/Workspaces"


# Check for python
PYVER=$(python3 --version)
if [ $? -ne 0 ]
then
	echo "Python3 not installed.  Please intall python3 and ensure it is in PATH"
	exit -2
fi
echo "Using $PYVER"

# Check for pyqt5
python3 -c "import PyQt5"
if [ $? -ne 0 ]
then
	# Check for pip
	PIPVER=$(pip3 --version)
	if [ $? -ne 0 ]
	then
		echo "pip3 not installed.  Please intall pip3 and ensure it is in PATH"
	exit -2
	fi
	echo "Using $PIPVER"
	# Install pyqt5
	echo "PyQt5 not installed. Installing with pip3"
	pip3 install PyQt5
fi


echo "Creating workspace dir"
mkdir $WSDIR
sudo chown root:root $WSDIR
sudo chmod u=rwx,g=rx,o=rx $WSDIR
echo "workspace dir at '$WSDIR'"

echo ""
echo "***"
echo "Workspace owned by root.  Executables and subdirs should also be owned by
root, to avoid potential security issues."
echo "***"
echo ""


echo "Installing script to '$INSTALLDIR'"
sudo mkdir $INSTALLDIR
sudo cp -r .. $INSTALLDIR

if [ $? -ne 0 ]
then 
	echo "Error copying files to $INSTALLDIR"
	exit -3

fi

echo "Adding executable to PATH ('/usr/bin')"
sudo cp ./Workspaces_unix /usr/bin/workspaces

GNOMEVER=$(gnome-shell --version)
if [ $? -eq 0 ]
then
	echo "Found $GNOMEVER"
	echo "Adding desktop entry to '/usr/share/applications'"
	sudo cp Gnome/workspace_launcher.desktop /usr/share/applications/
fi
